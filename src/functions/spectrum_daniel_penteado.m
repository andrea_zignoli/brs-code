function [Bm,Cm,fout] = spectrum_daniel_penteado(MM)

% the originla function has been written by D. Penteado
% this is a modified version of A. Zignoli Unitn (Aug '19)

%% load the data form input matrix (raw data [time|ibi|sbp])
t = MM(:,1);
p = MM(:,2);
f = MM(:,3);

%% interpolate in time with spline @10 Hz
fc  = 10; % Hz sampling frequency
Tc  = 1/fc; % s sampling time
% t_i = [0:Tc:t(end)]';
t_i = [0:Tc:t(end)]';
p_i = spline(t,p,t_i);
f_i = spline(t,f,t_i);

%% computing spectrum with:
L = 2^9; % (i.e. 2^9)
% overlapping 50% and hanning window to smooth
% it follows that the output frequency is from 0 to fc/2
[B,fw1,~] = specgram(p_i,L,fc,hanning(L),L/2);
[C,fw2,~] = specgram(f_i,L,fc,hanning(L),L/2);

B = B.*conj(B);
C = C.*conj(C);

%% compute output elements
Bm      = mean(B,2);
Cm      = mean(C,2);
fout    = fw1;

