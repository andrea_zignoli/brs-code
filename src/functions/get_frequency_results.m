function MM = get_frequency_results(structure)

% calculate the size of the structure
% (number of individuals and number of frequency points)
[n_subj, ~] = size(structure.Mag);
% initialize vectors
Mag_LF          = zeros(n_subj,1);
Mag_HF          = zeros(n_subj,1);
Mag_VLF         = zeros(n_subj,1);
Mag_LFss        = zeros(n_subj,1);
Mag_LF_std      = zeros(n_subj,1);
Mag_HF_std      = zeros(n_subj,1);
Mag_VLF_std     = zeros(n_subj,1);
Mag_LFss_std    = zeros(n_subj,1);
phase_LF        = zeros(n_subj,1);
phase_HF        = zeros(n_subj,1);
phase_VLF       = zeros(n_subj,1);
phase_LFss      = zeros(n_subj,1);
phase_LF_std    = zeros(n_subj,1);
phase_HF_std    = zeros(n_subj,1);
phase_VLF_std   = zeros(n_subj,1);
phase_LFss_std  = zeros(n_subj,1);
coh_LF          = zeros(n_subj,1);
coh_HF          = zeros(n_subj,1);
coh_VLF         = zeros(n_subj,1);
coh_LFss        = zeros(n_subj,1);
coh_LF_std      = zeros(n_subj,1);
coh_HF_std      = zeros(n_subj,1);
coh_VLF_std     = zeros(n_subj,1);
coh_LFss_std    = zeros(n_subj,1);

% find averages and SDs at different frequency ranges @coherence > min_coh
min_coh = 0;
% For the range of frequencies please check
% "Arterial-cardiac baroreflex function: insights from repeated squat-stand
% maneuvers", Zhang et al. 2009, Am J Physiol Regul Integr Comp Physiol
VLF     = [0.0078,0.05]; % Hz
LF      = [0.05,0.15]; % Hz
HF      = [0.15,0.35]; % Hz

% VLF     = [0,0.04]; % Hz
% LF      = [0.04,0.15]; % Hz
% HF      = [0.15,0.4]; % Hz

% for the SS test, we take only 0.031<f<0.078 Hz
% in Nov19 this has been amended by AZ: we include this frequency also in rest
% condition
LFss   = [0.031, 0.078];

%% squat stand tests -> only 0.031-0.078 Hz
for i = 1: n_subj
    
    % magnitude
    Mag_LF(i)       = mean(structure.Mag(i,structure.omega>LF(1) & structure.omega<LF(2) & structure.coherence(i,:)' > min_coh));
    Mag_HF(i)       = mean(structure.Mag(i,structure.omega>HF(1) & structure.omega<HF(2) & structure.coherence(i,:)' > min_coh));
    Mag_VLF(i)      = mean(structure.Mag(i,structure.omega>VLF(1) & structure.omega<VLF(2) & structure.coherence(i,:)' > min_coh));
    Mag_LFss(i)     = mean(structure.Mag(i,structure.omega>LFss(1) & structure.omega<LFss(2) & structure.coherence(i,:)' > min_coh));
    Mag_LF_std(i)   = std(structure.Mag(i,structure.omega>LF(1) & structure.omega<LF(2) & structure.coherence(i,:)' > min_coh));
    Mag_HF_std(i)   = std(structure.Mag(i,structure.omega>HF(1) & structure.omega<HF(2) & structure.coherence(i,:)' > min_coh));
    Mag_VLF_std(i)  = std(structure.Mag(i,structure.omega>VLF(1) & structure.omega<VLF(2) & structure.coherence(i,:)' > min_coh));
    Mag_LFss_std(i) = std(structure.Mag(i,structure.omega>LFss(1) & structure.omega<LFss(2) & structure.coherence(i,:)' > min_coh));
    % phase
    phase_LF(i)       = mean(structure.phase(i,structure.omega>LF(1) & structure.omega<LF(2) & structure.coherence(i,:)' > min_coh));
    phase_HF(i)       = mean(structure.phase(i,structure.omega>HF(1) & structure.omega<HF(2) & structure.coherence(i,:)' > min_coh));
    phase_VLF(i)      = mean(structure.phase(i,structure.omega>VLF(1) & structure.omega<VLF(2) & structure.coherence(i,:)' > min_coh));
    phase_LFss(i)     = mean(structure.phase(i,structure.omega>LFss(1) & structure.omega<LFss(2) & structure.coherence(i,:)' > min_coh));
    phase_LF_std(i)   = std(structure.phase(i,structure.omega>LF(1) & structure.omega<LF(2) & structure.coherence(i,:)' > min_coh));
    phase_HF_std(i)   = std(structure.phase(i,structure.omega>HF(1) & structure.omega<HF(2) & structure.coherence(i,:)' > min_coh));
    phase_VLF_std(i)  = std(structure.phase(i,structure.omega>VLF(1) & structure.omega<VLF(2) & structure.coherence(i,:)' > min_coh));
    phase_LFss_std(i) = std(structure.phase(i,structure.omega>LFss(1) & structure.omega<LFss(2) & structure.coherence(i,:)' > min_coh));
    % coherence
    coh_LF(i)       = mean(structure.coherence(i,structure.omega>LF(1) & structure.omega<LF(2)));
    coh_HF(i)       = mean(structure.coherence(i,structure.omega>HF(1) & structure.omega<HF(2)));
    coh_VLF(i)      = mean(structure.coherence(i,structure.omega>VLF(1) & structure.omega<VLF(2)));
    coh_LFss(i)     = mean(structure.coherence(i,structure.omega>LFss(1) & structure.omega<LFss(2)));
    coh_LF_std(i)   = std(structure.coherence(i,structure.omega>LF(1) & structure.omega<LF(2)));
    coh_HF_std(i)   = std(structure.coherence(i,structure.omega>HF(1) & structure.omega<HF(2)));
    coh_VLF_std(i)  = std(structure.coherence(i,structure.omega>VLF(1) & structure.omega<VLF(2)));
    coh_LFss_std(i) = std(structure.coherence(i,structure.omega>LFss(1) & structure.omega<LFss(2)));
       
end

%% collect results
% collect the result matrix
MM(:,1)   = Mag_LF;
MM(:,2)   = Mag_LF_std;
MM(:,3)   = Mag_HF;
MM(:,4)   = Mag_HF_std;
MM(:,5)   = Mag_VLF;
MM(:,6)   = Mag_VLF_std;
MM(:,7)   = Mag_LFss;
MM(:,8)   = Mag_LFss_std;
MM(:,9)   = phase_LF;
MM(:,10)  = phase_LF_std;
MM(:,11)  = phase_HF;
MM(:,12)  = phase_HF_std;
MM(:,13)  = phase_VLF;
MM(:,14)  = phase_VLF_std;
MM(:,15)  = phase_LFss;
MM(:,16)  = phase_LFss_std;
MM(:,17)  = coh_LF;
MM(:,18)  = coh_LF_std;
MM(:,19)  = coh_HF;
MM(:,20)  = coh_HF_std;
MM(:,21)  = coh_VLF;
MM(:,22)  = coh_VLF_std;
MM(:,23)  = coh_LFss;
MM(:,24)  = coh_LFss_std;

