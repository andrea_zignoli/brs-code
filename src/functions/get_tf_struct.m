function main_tf_struct = get_tf_struct(data, file_name)

% initialise struct
main_tf_struct = struct();
% import and save the name of the file (subject)
sbj_name = extractBefore(file_name,'_');
sbj_name(1) = upper(sbj_name(1));
main_tf_struct.name = sbj_name;
% pre data
main_tf_struct.time = data.time;
% output
main_tf_struct.ibi = data.ibi * 1000; % - mean(data.ibi * 1000);
% input
main_tf_struct.sbp = data.sbp; % - mean(data.sbp);

% re-sampling
Ts = 1/5; % s (5 Hz)
main_tf_struct.fc = 5; % Hz

% filtering
main_tf_struct.ibi_F = main_tf_struct.ibi;
main_tf_struct.sbp_F = main_tf_struct.sbp;

% interpolate
main_tf_struct.time_i = main_tf_struct.time(1):Ts:main_tf_struct.time(end);
main_tf_struct.ibi_i  = interp1(main_tf_struct.time, main_tf_struct.ibi_F, main_tf_struct.time_i, 'pchip');
main_tf_struct.sbp_i  = interp1(main_tf_struct.time, main_tf_struct.sbp_F, main_tf_struct.time_i, 'pchip');

% zero mean
main_tf_struct.ibi_i_exp = main_tf_struct.ibi_i;
main_tf_struct.sbp_i_exp = main_tf_struct.sbp_i;

% define new data and new system 
main_tf_struct.ddata    = iddata(main_tf_struct.ibi_i_exp',main_tf_struct.sbp_i_exp',Ts);
main_tf_struct.sys      = tfest(main_tf_struct.ddata,2,1);


