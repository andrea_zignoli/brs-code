function MM = get_PSD_results(structure)

% this script calculates PSD of SBP and IBI in the different frequency
% domains (VLF LF e HF). Then BRS at VLF, LF e HF are computed

% calculate the size of the structure (number of individuals and number of frequency points)
[n_subj, ~] = size(structure.PSD.ibi);
[~, ~]      = size(structure.coherence);
% initialize vectors
ibi_PSD_LF          = zeros(n_subj,1);
ibi_PSD_HF          = zeros(n_subj,1);
ibi_PSD_VLF         = zeros(n_subj,1);
ibi_PSD_LFss        = zeros(n_subj,1);
ibi_PSD_LF_std      = zeros(n_subj,1);
ibi_PSD_HF_std      = zeros(n_subj,1);
ibi_PSD_VLF_std     = zeros(n_subj,1);
ibi_PSD_LFss_std    = zeros(n_subj,1);
sbp_PSD_LF          = zeros(n_subj,1);
sbp_PSD_HF          = zeros(n_subj,1);
sbp_PSD_VLF         = zeros(n_subj,1);
sbp_PSD_LFss        = zeros(n_subj,1);
sbp_PSD_LF_std      = zeros(n_subj,1);
sbp_PSD_HF_std      = zeros(n_subj,1);
sbp_PSD_VLF_std     = zeros(n_subj,1);
sbp_PSD_LFss_std    = zeros(n_subj,1);
brs_PSD_LF          = zeros(n_subj,1);
brs_PSD_HF          = zeros(n_subj,1);
brs_PSD_VLF         = zeros(n_subj,1);
brs_PSD_LFss        = zeros(n_subj,1);
brs_PSD_LF_std      = zeros(n_subj,1);
brs_PSD_HF_std      = zeros(n_subj,1);
brs_PSD_VLF_std     = zeros(n_subj,1);
brs_PSD_LFss_std    = zeros(n_subj,1);

% find averages and SDs at different frequency ranges @coherence > min_coh
min_coh = 0;
% For the range of frequencies please check
% "Arterial-cardiac baroreflex function: insights from repeated squat-stand
% maneuvers", Zhang et al. 2009, Am J Physiol Regul Integr Comp Physiol
VLF     = [0.0078,0.05]; % Hz
LF      = [0.05,0.15]; % Hz
HF      = [0.15,0.35]; % Hz

% VLF     = [0,0.04]; % Hz
% LF      = [0.04,0.15]; % Hz
% HF      = [0.15,0.4]; % Hz

% for the SS test, we take only 0.031<f<0.078 Hz
% in Nov19 this has been amended by AZ: we include this frequency also in rest
% condition
LFss   = [0.031, 0.078];

%% squat stand tests -> only 0.031-0.078 Hz!!!

for i = 1: n_subj
    
    % PSD ibi
    ibi_PSD_LF(i)       = mean(structure.PSD.ibi(i,structure.PSD.f>LF(1) & structure.PSD.f<LF(2))) * (LF(2) - LF(1));
    ibi_PSD_HF(i)       = mean(structure.PSD.ibi(i,structure.PSD.f>HF(1) & structure.PSD.f<HF(2))) * (HF(2) - HF(1));
    ibi_PSD_VLF(i)      = mean(structure.PSD.ibi(i,structure.PSD.f>VLF(1) & structure.PSD.f<VLF(2))) * (VLF(2) - VLF(1));
    ibi_PSD_LFss(i)     = mean(structure.PSD.ibi(i,structure.PSD.f>LFss(1) & structure.PSD.f<LFss(2))) * (LFss(2) - LFss(1));
    ibi_PSD_LF_std(i)   = std(structure.PSD.ibi(i,structure.PSD.f>LF(1) & structure.PSD.f<LF(2))) * (LF(2) - LF(1));
    ibi_PSD_HF_std(i)   = std(structure.PSD.ibi(i,structure.PSD.f>HF(1) & structure.PSD.f<HF(2))) * (HF(2) - HF(1));
    ibi_PSD_VLF_std(i)  = std(structure.PSD.ibi(i,structure.PSD.f>VLF(1) & structure.PSD.f<VLF(2))) * (VLF(2) - VLF(1));
    ibi_PSD_LFss_std(i) = std(structure.PSD.ibi(i,structure.PSD.f>LFss(1) & structure.PSD.f<LFss(2))) * (LFss(2) - LFss(1));
    % PSD sbp
    sbp_PSD_LF(i)       = mean(structure.PSD.sbp(i,structure.PSD.f>LF(1) & structure.PSD.f<LF(2))) * (LF(2) - LF(1));
    sbp_PSD_HF(i)       = mean(structure.PSD.sbp(i,structure.PSD.f>HF(1) & structure.PSD.f<HF(2))) * (HF(2) - HF(1));
    sbp_PSD_VLF(i)      = mean(structure.PSD.sbp(i,structure.PSD.f>VLF(1) & structure.PSD.f<VLF(2))) * (VLF(2) - VLF(1));
    sbp_PSD_LFss(i)     = mean(structure.PSD.sbp(i,structure.PSD.f>LFss(1) & structure.PSD.f<LFss(2))) * (LFss(2) - LFss(1));
    sbp_PSD_LF_std(i)   = std(structure.PSD.sbp(i,structure.PSD.f>LF(1) & structure.PSD.f<LF(2))) * (LF(2) - LF(1));
    sbp_PSD_HF_std(i)   = std(structure.PSD.sbp(i,structure.PSD.f>HF(1) & structure.PSD.f<HF(2))) * (HF(2) - HF(1));
    sbp_PSD_VLF_std(i)  = std(structure.PSD.sbp(i,structure.PSD.f>VLF(1) & structure.PSD.f<VLF(2))) * (VLF(2) - VLF(1));
    sbp_PSD_LFss_std(i) = std(structure.PSD.sbp(i,structure.PSD.f>LFss(1) & structure.PSD.f<LFss(2))) * (LFss(2) - LFss(1));
    % BRS
    brs                 = sqrt(structure.PSD.ibi(i,:)./structure.PSD.sbp(i,:));
    brs_PSD_LF(i)       = mean(brs(structure.PSD.f>LF(1) & structure.PSD.f<LF(2)));
    brs_PSD_HF(i)       = mean(brs(structure.PSD.f>HF(1) & structure.PSD.f<HF(2)));
    brs_PSD_VLF(i)      = mean(brs(structure.PSD.f>VLF(1) & structure.PSD.f<VLF(2)));
    brs_PSD_LFss(i)     = mean(brs(structure.PSD.f>LFss(1) & structure.PSD.f<LFss(2)));
    brs_PSD_LF_std(i)   = std(brs(structure.PSD.f>LF(1) & structure.PSD.f<LF(2)));
    brs_PSD_HF_std(i)   = std(brs(structure.PSD.f>HF(1) & structure.PSD.f<HF(2)));
    brs_PSD_VLF_std(i)  = std(brs(structure.PSD.f>VLF(1) & structure.PSD.f<VLF(2)));
    brs_PSD_LFss_std(i) = std(brs(structure.PSD.f>LFss(1) & structure.PSD.f<LFss(2)));
    
end

%% collect the result matrix

MM(:,1)   = ibi_PSD_LF;
MM(:,2)   = ibi_PSD_LF_std;
MM(:,3)   = ibi_PSD_HF;
MM(:,4)   = ibi_PSD_HF_std;
MM(:,5)   = ibi_PSD_VLF;
MM(:,6)   = ibi_PSD_VLF_std;
MM(:,7)   = ibi_PSD_LFss;
MM(:,8)   = ibi_PSD_LFss_std;
MM(:,9)   = sbp_PSD_LF;
MM(:,10)  = sbp_PSD_LF_std;
MM(:,11)  = sbp_PSD_HF;
MM(:,12)  = sbp_PSD_HF_std;
MM(:,13)  = sbp_PSD_VLF;
MM(:,14)  = sbp_PSD_VLF_std;
MM(:,15)  = sbp_PSD_LFss;
MM(:,16)  = sbp_PSD_LFss_std;
MM(:,17)  = brs_PSD_LF;
MM(:,18)  = brs_PSD_LF_std;
MM(:,19)  = brs_PSD_HF;
MM(:,20)  = brs_PSD_HF_std;
MM(:,21)  = brs_PSD_VLF;
MM(:,22)  = brs_PSD_VLF_std;
MM(:,23)  = brs_PSD_LFss;
MM(:,24)  = brs_PSD_LFss_std;

