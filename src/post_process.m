%%%%%%%% --------- --------- %%%%%%%%
% Baroreflex project
% A. Zignoli
% University of Trento
%%%%%%%% --------- --------- %%%%%%%%

%% preamble
clc
clear all
close all

addpath(genpath('functions'))
addpath(genpath('output'))
addpath(genpath('txt'))

%% LOAD FILE
results_file = uigetfile('output/.mat', 'Select results');
load(results_file);

%% load the magnitude for every participant
MM_TF           = get_frequency_results(results);
MM_PSD          = get_PSD_results(results); 
[n_subj_TF, ~]  = size(MM_TF);
[n_subj_PSD, ~] = size(MM_PSD);

%% write pre results on a txt file (PSD)
fileID = fopen(['txt/', results_file(1:end-4), '_PSD.txt'],'w');
fprintf(fileID,'N\tSBJ\tCond\tKind\tIBI-LF\tIBI-LF-std\tIBI-HF\tIBI-HF-std\tIBI-VLF\tIBI-VLF-std\tIBI-LFss\tIBI-LFss-std\tSBP-LF\tSBP-LF-std\tSBP-HF\tSBP-HF-std\tSBP-VLF\tSBP-VLF-std\tSBP-LFss\tSBP-LFss-std\tBRS-LF\tBRS-LF-std\tBRS-HF\tBRS-HF-std\tBRS-VLF\tBRS-VLF-std\tBRS-LFss\tBRS-LFss-std\n');
formatSpec = '%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\n';
for i = 1 : n_subj_PSD
    fprintf(fileID, '%d\t', i);
    fprintf(fileID, '%s\t', char(results.name{i}));
    fprintf(fileID, '%s\t', char(results.condition{i}));
    fprintf(fileID, '%s\t', char(results.kind{i}));
    fprintf(fileID, formatSpec, MM_PSD(i,:));
end
fclose(fileID);

%% write pre results on a txt file (tf)
fileID = fopen(['txt/', results_file(1:end-4), '_TF.txt'],'w');
fprintf(fileID,'N\tSBJ\tCond\tKind\tBRS-LF\tBRS-LF-std\tBRS-HF\tBRS-HF-std\tBRS-VLF\tBRS-VLF-std\tBRS-LFss\tBRS-LFss-std\tphase-LF\tphase-LF-std\tphase-HF\tphase-HF-std\tphase-VLF\tphase-VLF-std\tphase-LFss\tphase-LFss-std\tcoh-LF\tcoh-LF-std\tcoh-HF\tcoh-HF-std\tcoh-VLF\tcoh-VLF-std\tcoh-LFss\tcoh-LFss-std\n');
formatSpec = '%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%4.3f\n';
for i = 1 : n_subj_TF
    fprintf(fileID, '%d\t', i);
    fprintf(fileID, '%s\t', char(results.name{i}));
    fprintf(fileID, '%s\t', char(results.condition{i}));
    fprintf(fileID, '%s\t', char(results.kind{i}));
    fprintf(fileID, formatSpec, MM_TF(i,:));
end
fclose(fileID);

