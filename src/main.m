%%%%%%%% --------- --------- %%%%%%%%
% Baroreflex project
% A. Zignoli
% University of Trento
%%%%%%%% --------- --------- %%%%%%%%

%% preamble
clc
clear all
close all

addpath(genpath('../data/'));
addpath(genpath('functions'));

%% data selection
% select the mat file
% mat files have a structure with time|ibi|sbp inside
files = uigetfile('../data/.mat', 'Select data files', 'MultiSelect', 'on');

if ~iscell(files)
    files = {files};
end %now filename is a cell array regardless of the number of selected files.

% loop the files
for i = 1:length(files)
    % load the files (multiple selection)
    load(files{i});
    
    % this depends on the structure of the files
    % file name is: subject_condition_testtype.mat
    s       = strsplit(files{i},'_');
    sbj     = s{1};
    cond    = s{2};
    kind    = [s{3} s{4}(1)];
    
    % print screen
    disp([num2str(i), ') loading ', files{i}, ' (', num2str(round(i/length(files)*100)), '%)']);
    % generate the structure
    SS = get_tf_struct(data, files{i});
    
    %% Daniel Penteado script (using specgram instead of fft)
    % please check:
    % https://in.mathworks.com/matlabcentral/answers/55224-compute-psd-from-fft-compared-with-spectrogram
    [B,C,f_out] = spectrum_daniel_penteado([SS.time SS.ibi SS.sbp]);
    
    % store the results in the main structure Penteado
    results.PSD_penteado.ibi(i,:)     = B;
    results.PSD_penteado.sbp(i,:)     = C;
    results.PSD_penteado.f            = f_out;
    
    % FC
    fc = SS.fc; % Hz
    % save the name
    results.name{i}         = sbj;
    results.condition{i}    = cond;
    results.kind{i}         = kind;
    % extract the data
    [num,den,Ts,sdnum,sdden] = tfdata(SS.sys);
    % load the matrix
    results.MM(i,:) = [num{1}, den{1}, sdnum{1}, sdden{1}];
    [EstH, EstF]    = tfestimate(SS.sbp_i_exp, SS.ibi_i_exp, [], [], [], 4);
    EstMag          = abs(EstH);
    EstPhase        = angle(EstH);
    EstOmega        = EstF;
    [cxy,EstF_c]    = mscohere(SS.sbp_i_exp, SS.ibi_i_exp, [], [], [], 4);
    
    % store the data
    results.omega           = [EstOmega(1):0.01:EstOmega(end)]';
    results.Mag(i,:)        = pchip(EstOmega,   EstMag,    EstOmega(1):0.01:EstOmega(end))';
    results.phase(i,:)      = pchip(EstOmega,   EstPhase,  EstOmega(1):0.01:EstOmega(end))';
    results.coherence(i,:)  = pchip(EstOmega,   abs(cxy),  EstOmega(1):0.01:EstOmega(end))';
    
    % save the idtf
    current_model           = ['sys_', num2str(i)];
    % results.(current_model) = SS.sys;
    
    % power spectra (PSD)
    % length of the segments: 128 [Iwasaki Zhang Dose-response cardiovascular adapt.]
    L  = 2^9;
    
    % cut the end and reshape so the rows are the number of segments
    ibi = SS.ibi_i(1:end-rem(length(SS.ibi_i), L/2));
    sbp = SS.sbp_i(1:end-rem(length(SS.sbp_i), L/2));
    
    % overlapping
    L_overlap   = L/2; % 50% overlapping
    % number of segments after overlapping
    n_segments  = length(SS.ibi_i(1:end - rem(length(SS.ibi_i), L_overlap)))/(L_overlap) - 1;
    
    % initialise
    ibi_segments = zeros(n_segments, L);
    sbp_segments = zeros(n_segments, L);
    
    % first line (no loop needed)
    ibi_segments(1,:) = ibi(1:L);
    sbp_segments(1,:) = sbp(1:L);
    
    for k = 2:n_segments
        
        ibi_segments(k,:) = ibi((k-1)*L/2+1:(k-1)*L/2+L);
        sbp_segments(k,:) = sbp((k-1)*L/2+1:(k-1)*L/2+L);
        
    end
     
    % segment by segment    
    for j = 1:n_segments
       
        % ibi
        ibi_dft             = fft(ibi_segments(j,:));
        ibi_dft             = ibi_dft(1:round(L/2+1));
        psd_ibi(j,:)        = (1/(fc * L)) * abs(ibi_dft).^2;
        psd_ibi(j,2:end-1)  = 2 * psd_ibi(j,2:end-1);
        % sbp
        sbp_dft             = fft(sbp_segments(j,:));
        sbp_dft             = sbp_dft(1:round(L/2+1));
        psd_sbp(j,:)        = (1/(fc * L)) * abs(sbp_dft).^2;
        psd_sbp(j,2:end-1)  = 2 * psd_sbp(j,2:end-1);
        
    end
    
    % frequency
    f       = 0:fc/L:fc/2;
    psd_ibi = mean(psd_ibi);
    psd_sbp = mean(psd_sbp);
    
    % store the results in the main structure
    results.PSD.ibi(i,:)     = psd_ibi;
    results.PSD.sbp(i,:)     = psd_sbp;
    results.PSD.f            = f;
   
    %% Plot single-sided amplitude spectrum.
    % please consider that if you un-comment the following lines you might
    % end up generating too many pictures 
% %     figure(1)
% %     subplot(1,2,1)
% %     title('PSD')
% %     xlabel('Frequency (Hz)')
% %     ylabel('|Y(f)|')
% %     hold on
% %     plot(f, psd_ibi, 'b')
% %     % plot(f_out, C/10^4, 'r')
% %     % plot(fper_ibi, psd_ibi_period/10^4, 'g')
% %     legend('ibi')
% %     xlim([0 0.5])
% %     subplot(1,2,2)
% %     hold on
% %     plot(f, psd_sbp, 'b')
% %     % plot(f_out, B/10^2, 'r')
% %     % plot(fper_sbp, psd_sbp_period/10^2, 'g')
% %     legend('sbp')
% %     title('PSD')
% %     xlabel('Frequency (Hz)')
% %     ylabel('|Y(f)|')
% %     % ylim([0 max(Y)])
% %     xlim([0 0.5])
% %     
    clear psd_ibi psd_sbp f
    
end

%% save the results in the output directory
save output/my_saved_results.mat results
disp('Results saved in a mat file in the output directory and now it is ready for post-process');

