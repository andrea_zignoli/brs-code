# BRS-code

## Intro

This repository contains the code that allows you to compute baroreflex sensitivity (BRS) parameters. 

We separate between transfer function (TF) and power spectral density (PSD) analyses. TF analyses include the calculation of the magnitude, the phase and the coherence. PSD analyses include the calculation of the PSD. 

Three different frequency ranges have been considered: 
1. very low frequency (VLF from 0.0078 to 0.05 Hz), 
2. low frequency (LF from 0.05 to 0.15 Hz) and 
3. high frequency (HF from 0.15 to 0.35 Hz). 
4. squat stand (ss) frequency (LFss from 0.031 to 0.078 Hz)

We mainly followed the methods detailed in [1] and [2], including the possibility to process more files with one upload and to specify the length of the segments and the overlapping ratio (%) between segments. However, [2] makes use of the *specgram* Matlab function [useful link](https://in.mathworks.com/matlabcentral/answers/55224-compute-psd-from-fft-compared-with-spectrogram). The same function is used in the [CardioSeries](http://www.danielpenteado.com/cardioseries) software [3]. The code presented here has been used to compute BRS indeces in [4] and [5].

## How to use the code

The *main.m* file is used to load the data and to create the structures. The *post_process.m* file is used to process the structures and to create the output text files. After running *main.m* a result structure is created and save in *output* directory. Post process starts with the request to the user to load the file locate in the *output* directory and saves two different text files (one for TF and one for PSD results) in the *txt* directory. 

### Data file structure

The data files must be supplied in *mat* format. In every *mat* file there must be a structure *data* with three fields: time|ibi|sbp. We do not provide the scripts to create these structures from other sources (e.g. from txt data files). 

### Data sample

We provide a directory *data* with sample files (in the form subject_condition_testtype.mat). These files can be used for test and debugging custom made codes. 

---

# Bibliography

[1] Zhang et al., Arterial-cardiac baroreflex function: insights from repeated squat-stand maneuvers, Am J Physiol Regul Integr Comp Physiol 297: R116–R123, 2009.

[2] Silva et al., Revisiting the Sequence Method for Baroreflex Analysis, Front Neurosci, 2019

[3] Dias et al., Correlation between RR, inter-systolic and inter-diastolic intervals and their differences for the analysis of spontaneous heart rate variability, Physiol Meas, 2016 

[4] Fornasiero et al., Cardiac Autonomic and Physiological Responses to Moderate-Intensity Exercise in Hypoxia, Int J Sp Med 40(14), 2019 

[5] Mourot et al., Similar cardiovascular and autonomic responses in trained type 1 diabetes mellitus and healthy participants in response to half marathon, Diab Res Clin Pract, 2020